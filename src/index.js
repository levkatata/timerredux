import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Timer from './components/timer/timer';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import TimerReducer from './components/features/reducer';

const store = configureStore({
  reducer: {
    timer: TimerReducer,
  },
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
      <Timer />
    </Provider>
);