import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './settings.module.css';
import { start } from '../features/reducer';
import { measures, startTimer } from '../features/helpers';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export function Settings() {
    const state = useSelector((state) => state.timer.value);
    const dispatch = useDispatch();
    const [count, setCount] = useState(0); //number of seconds to count
    const [measure, setMeasure] = useState(measures[0]);

    //Use toast component for displaying error messages
    const toastMsg = () => { toast.error('Number must be greater than 0!', {
        position: toast.POSITION.BOTTOM_CENTER,
        closeOnClick: true,
        autoClose: 5000
    }) };

    const onChangeMeasure = (e) => {
        setMeasure(e.target.value);
    }

    return (<div className={styles.divmain}>
        <div className={styles.divsettings}>
            <div>
                <label htmlFor="count">Enter timer value: </label>
                <input name="count"
                    type="number"
                    value={count} 
                    onChange={(e) => {setCount(e.target.value)}}/>
            </div>
            <div>
                <label>
                    <input name="measure"
                        type="radio"
                        value={measures[0]}
                        checked={measure === measures[0]}
                        onChange={onChangeMeasure}/>
                    {measures[0]} 
                </label>
                <label>
                    <input name="measure"
                        type="radio"
                        value={measures[1]}
                        checked={measure === measures[1]}
                        onChange={onChangeMeasure}/>
                    {measures[1]}
                </label>
            </div>
        </div>
        <button className={styles.btnstart}
            onClick={() => {
                if (count > 0) {
                    const intervalNumber = startTimer(dispatch);
                    dispatch(start(
                        {count: measure === measures[0] ? parseInt(count) : count * 60, 
                            iNumber: intervalNumber}));
                } else {
                    toast.clearWaitingQueue();
                    toastMsg();
                }
        }}>Start</button>
        <ToastContainer limit={1}/>
    </div>);
}