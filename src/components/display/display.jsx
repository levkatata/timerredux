import styles from './display.module.css';
import { useSelector, UseSelector } from "react-redux/es/hooks/useSelector";
import { useDispatch } from 'react-redux';
import { paused } from '../features/reducer';
import { stopTimer } from '../features/helpers';
import ringtone from '../../res/ringtone.mp3';
import { useEffect } from 'react';
import { useState } from 'react';

export function Display() {
    const state = useSelector( (state) => state.timer.value );
    const dispatch = useDispatch();
    const [audio] = useState(new Audio(ringtone));
  
    useEffect(() => {
        audio.addEventListener('ended', loopMusic);

        return () => { 
            audio.pause();
            audio.currentTime = 0;
            audio.removeEventListener('ended', loopMusic);
        };
    }, []);
  
    useEffect(() => {
      if (state.seconds <= 0) {
        stopTicks();
      } 
    }, [state.seconds]);
  
    const stopTicks = () => {
        stopTimer(state.iNumber);
        dispatch(paused());
        console.log("Start playing music");
        audio.play();
        console.dir(state);
    }
    const loopMusic = () => {
        audio.play();
    }

    const prepareTime = () => {
        if (state.seconds < 60) {
            return `${state.seconds} s`;
        } else {
            return `${Math.floor(state.seconds / 60)} m ${state.seconds % 60} s`;
        }
    }
  
    return (<>
        <div className={styles.divdisplay}>{prepareTime()}</div>
        <div className={styles.divprogress} style={{
            width: `${Math.floor((state.seconds / state.wholeTime) * 100)}%`}}/>
        </>);
}