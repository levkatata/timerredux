import { createSlice } from "@reduxjs/toolkit";
import { getState, saveState } from "./helpers";

export const initState = { seconds: undefined, isTimerStarted: false, isTimerPaused: false, iNumber: undefined };

export const timerSlice = createSlice({
    name: "timer",
    initialState: { value:  getState() },
    reducers: {
        start: (state, action) => {
            console.log("start action");
            state.value = { seconds: action.payload.count || state.value.seconds,
                isTimerStarted: true, isTimerPaused: false, iNumber: action.payload.iNumber,
                wholeTime: action.payload.count || state.value.wholeTime }
            saveState({...state.value, iNumber: undefined, isTimerPaused: true});
        },
        paused: (state) => {
            console.log("pause action");
            state.value = { seconds: state.value.seconds, isTimerStarted: true, isTimerPaused: true, iNumber: undefined, wholeTime: state.value.wholeTime }
            saveState(state.value);
        },
        cancel: (state) => {
            console.log("cancel action");
            state.value = { seconds: undefined, isTimerStarted: false, isTimerPaused: false, iNumber: undefined, wholeTime: undefined }
            saveState(state.value);
        },
        tick: (state) => {
            console.log("tick action");
            state.value = { seconds: state.value.seconds - 1, isTimerStarted: true, isTimerPaused: false, iNumber: state.value.iNumber, wholeTime: state.value.wholeTime }
            saveState({...state.value, iNumber: undefined, isTimerPaused: true});
        }
    }
});

export default timerSlice.reducer;
export const { start, paused, cancel, tick } = timerSlice.actions;