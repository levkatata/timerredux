import { initState, tick } from "./reducer";

export function startTimer(dispatcher) {
    const i = setInterval(() => {
        dispatcher(tick());
    }, 1000);
    console.log("Start " + i);
    return i;
}

export function stopTimer(iNumber) {
    console.log("Stop " + iNumber);
    clearInterval(iNumber);
}

const TIMER_KEY = "Redux_timer";
export function saveState(object) {
    localStorage.setItem(TIMER_KEY, JSON.stringify(object));
}
export function getState() {
    let state = localStorage.getItem(TIMER_KEY);
    if (state === null) {
        state = initState;
        saveState(state);
        return state; 
    }
    return JSON.parse(state);
}

export const measures = ["seconds", "minutes"];