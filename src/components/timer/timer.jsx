import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Control } from '../control/control';
import { Display } from '../display/display';
import { getState, startTimer, stopTimer } from '../features/helpers';
import { start } from '../features/reducer';
import { Settings } from '../settings/settings';
import styles from './timer.module.css';

function Timer() {
  const state = useSelector((state) => state.timer.value);

  return (
    <div className={styles.divmain}>
      {state.isTimerStarted ? 
        (<div className={styles.divcontainer}>
          <Display/>
          <Control/>
        </div>) 
        : 
        (<div className={styles.divcontainer}>
          <Settings/>
        </div>)
      }
    </div>
  );
}

export default Timer;
