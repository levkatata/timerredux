import styles from './control.module.css';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { start, cancel, paused } from '../features/reducer';
import { startTimer, stopTimer } from '../features/helpers';
import { useEffect } from 'react';

export function Control() {
    const timerState = useSelector((state) => state.timer.value);
    const dispatch = useDispatch();

    return (<div className={styles.divcontrol}>
        {
            timerState.seconds > 0 ? 
            (<button 
                onClick= {() => {
                if (!timerState.isTimerPaused) {
                    stopTimer(timerState.iNumber);
                    dispatch(paused());
                } else {
                    const iNumber = startTimer(dispatch);
                    dispatch(start({iNumber: iNumber}));
                }
                }}> 
                {!timerState.isTimerPaused ? "Pause" : "Resume"}
            </button>) 
            : ""
        }

        <button onClick={() => {
                if (!timerState.isTimerPaused) {
                    stopTimer(timerState.iNumber);
                }
                dispatch(cancel());
            }}>
            Cancel
        </button>
    </div>);
}